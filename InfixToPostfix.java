package week8;

import java.util.Scanner;
public class InfixToPostfix {
	static char [] stackIntoPos;
	static TreeCell [] stackMakeTree;
	static String ss="";
	static int indexIntoPos = 0,indexMakeTree=0;
	public static void main(String[]args){

		Scanner in = new Scanner(System.in);
		String s="1";
		while(!s.equals("0")){
			s = in.nextLine();
			stackIntoPos = new char[s.length()];
			stackMakeTree = new TreeCell [s.length()];
			String postfix = IntoPos(s);
			TreeCell tree = makeExpTree(postfix);
			System.out.print("IntoPos       : ");
			System.out.print(postfix);
			System.out.print("\nPrint Prefix  : ");
			printPrefix(tree);
			System.out.print("\nPrint Infix   : ");
			printInfix(tree);
			System.out.print("\nPrint Postfix : ");
			printPostfix(tree);
			System.out.print("\nPrint Value   : \n");
			System.out.println(eval(tree));
		}

	}
	/**
	 * Infix to Postfix
	 * Using isNumeric, pop
	 * @param infix get infix from the user
	 * @return postfix form of the infix
	 */
	public static String IntoPos(String infix){
		ss = "";
		indexIntoPos = 0;
		for(int i=0;i<infix.length();i++){
			if(infix.charAt(i)==' ') continue;
			if(isNumeric(infix.charAt(i)+"")){
				ss += infix.charAt(i)+"";
				popByMultiplication();
			}
			else{
				stackIntoPos[indexIntoPos++] = infix.charAt(i);
				popByParenthesis();
			}
		}
		popWhenEnd();
		return ss;
	}
	/**
	 * Check whether c is a number or not
	 * @param c is char from stack
	 * @return 1. true if c is a number
	 * 2.false if it is not
	 */
	public static boolean isNumeric(String c){
		try{
			int n = Integer.parseInt(c+"");
		}
		catch(NumberFormatException e){
			return false;
		}
		return true;
	}
	/**
	 *Pop when find multiply in stack 
	 */
	public static void popByMultiplication(){
		if(indexIntoPos>0)
			if(stackIntoPos[indexIntoPos-1]=='*'){
				while(true){
					if(indexIntoPos==0){
						stackIntoPos[0]=' ';
						break;
					}
					if(stackIntoPos[indexIntoPos-1]=='('){
						if(indexIntoPos>0) indexIntoPos--;
						else stackIntoPos[0] = ' ';
						break;
					}
					ss+= stackIntoPos[--indexIntoPos] +"";
				}
			}
	}
	/**
	 * Pop when find a close parenthesis
	 */
	public static void popByParenthesis(){
		if(indexIntoPos>0)
			if(stackIntoPos[indexIntoPos-1]==')'){
				indexIntoPos--;

				while(true){
					if(indexIntoPos==0){
						stackIntoPos[0]=' ';
						break;
					}
					if(stackIntoPos[indexIntoPos-1]=='('){
						if(indexIntoPos>0) indexIntoPos--;
						else stackIntoPos[0] = ' ';
						break;
					}
					ss += stackIntoPos[--indexIntoPos] + "";
				}
			}

	}
	/**
	 * Pop at the end of loop in IntoPos
	 */
	public static void popWhenEnd(){
		if(indexIntoPos==0){
			ss+= stackIntoPos[0];
		}
		else{
			for(int i =indexIntoPos-1;i>=0;i--){
				ss+=stackIntoPos[i];
			}
		}
	}

	/**
	 * Make Tree form postfix
	 * @param postfix
	 * @return TreeCell of that postfix
	 */
	public static TreeCell makeExpTree(String postfix){
		indexMakeTree = 0;
		for(int i =0;i<postfix.length();i++){
			if(postfix.charAt(i)==' ') continue;
			if(isNumeric(postfix.charAt(i)+"")){
				stackMakeTree[indexMakeTree++] = new TreeCell(postfix.charAt(i)+"",null,null);
			}
			else{
				TreeCell right = stackMakeTree[--indexMakeTree];
				TreeCell left = stackMakeTree[--indexMakeTree];
				TreeCell node = new TreeCell(postfix.charAt(i)+"", left, right);
				stackMakeTree[indexMakeTree++] = node;
			}
		}
		return stackMakeTree[0];
	}

	//	public static void makeParenthesisInTree(TreeCell node){
	//		TreeCell leftTemp = node.left.left;
	//		TreeCell rightTemp = node.right.right;
	//		TreeCell paraClose =new TreeCell(')',leftTemp,null);
	//		TreeCell paraOpen = new TreeCell('(',node,null);
	//		stackMakeTree[indexMakeTree++] = paraOpen;
	//	}
	public static void printPrefix(TreeCell root){
		TreeCell.preorder(root);
	}

	public static void printPostfix(TreeCell root){
		TreeCell.postorder(root);
	}

	public static void printInfix(TreeCell root){
		TreeCell.inorder(root);
	}

	public static void printTree(TreeCell root, int level ) {
		if (root != null) {
			printTree(root.right, level+1);
			for (int i=0; i<level; i++) System.out.print(" ");
			System.out.println(root.datum);
			System.out.println();
			printTree(root.left, level+1);
		}
	} 

	public static int eval(TreeCell root){
		int left_result, right_result;
		if (isNumeric(root.datum)) return Integer.parseInt(root.datum);
		left_result = eval(root.left);
		right_result = eval(root.right);
		switch (root.datum) {
		case "+" : 
			System.out.println("+ "+left_result +" "+ right_result);
			return left_result + right_result;
		case "*" : 
			System.out.println("* "+left_result +" "+ right_result);			
			return left_result * right_result;
		case "-" : 
			System.out.println("- "+left_result +" "+ right_result);			
			return left_result - right_result;
		case "/" : 
			System.out.println("/ "+left_result +" "+ right_result);			
			return left_result / right_result;
		default: 
			return 0;
		}

	}

}
