package week8;

public class TreeCell {
	String datum;
	TreeCell left,right;

	public TreeCell(String x, TreeCell lft,
			TreeCell rgt) {
		datum = x;
		left = lft;
		right = rgt;
	}

	public static void inorder(TreeCell root) {
		if (root != null) {
			if(root.datum.equals("*")||root.datum.equals("/")){
				helpPrintParenthesis(root,1);
			}
			else 
				inorder(root.left);
			System.out.print(root.datum + " ");
			if(root.datum.equals("*")||root.datum.equals("/")){
				helpPrintParenthesis(root, 2);
			}
			else 
				inorder(root.right);
		}
	}

	public static void postorder(TreeCell root) {
		if (root != null) {
			postorder(root.left);
			postorder(root.right);
			if(!root.datum.equals("(")&&!root.datum.equals(")"))
				System.out.print(root.datum + " "); 
		}
	} 
	public static void preorder(TreeCell root) {
		if (root != null) {
			if(!root.datum.equals("(")&&!root.datum.equals(")"))
				System.out.print(root.datum + " ");
			preorder(root.left);
			preorder(root.right);
		}
	} 


	public TreeCell getLeft(){
		return left;
	}

	public TreeCell getRight(){
		return left;
	}

	public void setLeft(TreeCell x){
		left = x;
	}

	public void setRight(TreeCell x){
		right = x;
	}
	public static void helpPrintParenthesis(TreeCell root,int i){
		if(i==1){
			System.out.print("( ");
			if(root.left.left!=null||root.right.right!=null)
				System.out.print("( ");
			inorder(root.left);
			if(root.left.left!=null||root.right.right!=null)
				System.out.print(") ");
		}
		else{
			if(root.left.left!=null||root.right.right!=null)
				System.out.print("( ");
			inorder(root.right);
			if(root.left.left!=null||root.right.right!=null)
				System.out.print(") ");
			System.out.print(") ");
		}
	}
}
