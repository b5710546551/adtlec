package week8;

import static org.junit.Assert.*;

import org.junit.Test;

public class InfixToPostfixTest {

	@Test
	public void testIsNumeric() {
		assertTrue(InfixToPostfix.isNumeric('1'));
		assertFalse(InfixToPostfix.isNumeric('a'));
	}

}
